/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.patterns.rules;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measurement;
import de.unibonn.realkd.common.workspace.SerialForm;
import de.unibonn.realkd.common.workspace.Workspace;
import de.unibonn.realkd.patterns.DefaultPattern;
import de.unibonn.realkd.patterns.MeasurementProcedure;
import de.unibonn.realkd.patterns.PatternBuilder;
import de.unibonn.realkd.patterns.PatternDescriptor;

/**
 * Provides factory method for the construction of association rules.
 * 
 * @author Mario Boley
 * @author Sandy Moens
 * 
 * @since 0.1.2
 * 
 * @version 0.4.0
 *
 */
public class AssociationRules {

	public static AssociationRule create(RuleDescriptor descriptor, List<MeasurementProcedure<? extends Measure,? super PatternDescriptor>> additionalProcedures) {

		List<Measurement> measurements = new ArrayList<>();
		
		List<MeasurementProcedure<? extends Measure,? super PatternDescriptor>> procedures = 
				additionalProcedures.stream().filter(p -> p.isApplicable(descriptor)).collect(Collectors.toList());
		
		measurements.addAll(procedures.stream().map(p -> p.perform(descriptor)).collect(Collectors.toList()));
		
		return new AssociationRuleImplementation(descriptor, measurements, ImmutableList.copyOf(procedures));
	}

	private static class AssociationRuleImplementation extends DefaultPattern<RuleDescriptor> implements AssociationRule {

		private final List<MeasurementProcedure<? extends Measure,? super PatternDescriptor>> proceduresBackup;

		private AssociationRuleImplementation(RuleDescriptor description, List<Measurement> measurements,
				List<MeasurementProcedure<? extends Measure,? super PatternDescriptor>> procedures) {
			super(description.getPropositionalLogic().population(), description, measurements);
			this.proceduresBackup = ImmutableList.copyOf(procedures);
		}

		@Override
		public RuleDescriptor descriptor() {
			return (RuleDescriptor) super.descriptor();
		}

		@Override
		public SerialForm<AssociationRule> serialForm() {
			return new AssociationRuleBuilderImplementation(descriptor().serialForm(), proceduresBackup);
		}

	}
	
	private static class AssociationRuleBuilderImplementation implements PatternBuilder<RuleDescriptor,AssociationRule> {

		@JsonProperty("descriptor")
		private final SerialForm<RuleDescriptor> descriptor;

		@JsonProperty("additionalMeasurementProcedures")
		private final List<MeasurementProcedure<? extends Measure,? super PatternDescriptor>> procedures;

		@JsonCreator
		public AssociationRuleBuilderImplementation(
				@JsonProperty("descriptor") SerialForm<RuleDescriptor> descriptorBuilder,
				@JsonProperty("additionalMeasurementProcedures") List<MeasurementProcedure<? extends Measure,? super PatternDescriptor>> procedures) {
			this.procedures = new ArrayList<>(procedures);
			this.descriptor = descriptorBuilder;
		}

		@Override
		public synchronized AssociationRule build(Workspace workspace) {
			RuleDescriptor ruleDescriptor = descriptor.build(workspace);
			return AssociationRules.create(ruleDescriptor, procedures);
		}

		@Override
		public synchronized SerialForm<RuleDescriptor> descriptor() {
			return descriptor;
		}

		// @Override
		// public List<MeasurementProcedure> getMeasurementProcedures() {
		// return ImmutableList.copyOf(procedures);
		// }

	}

}
