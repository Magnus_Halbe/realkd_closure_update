/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.common;

import static de.unibonn.realkd.common.measures.Measures.measure;

import java.io.IOException;
import java.util.logging.Logger;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTyping;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.jsontype.TypeDeserializer;
import com.fasterxml.jackson.databind.jsontype.TypeSerializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.guava.GuavaModule;

import de.unibonn.realkd.algorithms.ComputationMeasure;
import de.unibonn.realkd.common.base.Identifiable;
import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.measures.Measure;
import de.unibonn.realkd.common.measures.Measures;
import de.unibonn.realkd.patterns.emm.ModelDeviationMeasure;

/**
 * <p>
 * Provides functionality for the serialization/deserialization of objects as
 * json string. Currently this encapsulates Jackson to achieve this. Previous
 * GSON implementation was removed because GSON behaved unpredictable with
 * abstract generic types.
 * <p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.6.0
 *
 */
public class JsonSerialization {

	static final Logger LOGGER = Logger.getLogger(JsonSerialization.class.getName());

	private static class IdentifierSerializer extends JsonSerializer<Identifier> {

		@Override
		public void serialize(Identifier identifier, JsonGenerator generator, SerializerProvider serializerProvider)
				throws IOException {
			generator.writeString(identifier.toString());
		}

	}

	private static class IdentifierDeserializer extends JsonDeserializer<Identifier> {

		@Override
		public Identifier deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			return Identifier.identifier(p.readValueAs(String.class));
		}

	}

	private static class MeasureSerializer extends JsonSerializer<Measure> {
		@Override
		public void serialize(Measure value, JsonGenerator gen, SerializerProvider serializers)
				throws IOException, JsonProcessingException {
			if (value instanceof Identifiable) {
				gen.writeString(((Identifiable) value).id().toString());
			} else {
				gen.writeString(value.toString());
			}
		}

		@Override
		public void serializeWithType(Measure value, JsonGenerator gen, SerializerProvider provider,
				TypeSerializer typeSer) throws IOException, JsonProcessingException {
			serialize(value, gen, provider);
		}
	}

	private static class MeasureDeserializer extends JsonDeserializer<Measure> {

		@Override
		public Measure deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			return measure(Identifier.identifier(p.readValueAs(String.class)));
		}

		@Override
		public Measure deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer e)
				throws IOException {
			return measure(Identifier.identifier(p.readValueAs(String.class)));
		}

	}

	private static class ModelDeviationMeasureDeserializer extends JsonDeserializer<ModelDeviationMeasure> {

		@Override
		public ModelDeviationMeasure deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			Identifier id = Identifier.identifier(p.readValueAs(String.class));
			return Measures.modelDeviationMeasure(id);
		}

		@Override
		public ModelDeviationMeasure deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer e)
				throws IOException {
			return deserialize(p, ctxt);
		}

	}

	private static class ComputationMeasureDeserializer extends JsonDeserializer<ComputationMeasure> {

		@Override
		public ComputationMeasure deserialize(JsonParser p, DeserializationContext ctxt)
				throws IOException, JsonProcessingException {
			return Measures.computationMeasure(Identifier.identifier(p.readValueAs(String.class)));
		}

		@Override
		public ComputationMeasure deserializeWithType(JsonParser p, DeserializationContext ctxt, TypeDeserializer e)
				throws IOException {
			return deserialize(p, ctxt);
		}

	}

	private static final SimpleModule REALKD_MODULE = new SimpleModule();
	static {
		// use toString serializer instead?!
		REALKD_MODULE.addSerializer(Identifier.class, new IdentifierSerializer());
		REALKD_MODULE.addDeserializer(Identifier.class, new IdentifierDeserializer());
		REALKD_MODULE.addSerializer(Measure.class, new MeasureSerializer());
		REALKD_MODULE.addDeserializer(Measure.class, new MeasureDeserializer());
		// REALKD_MODULE.addSerializer(ModelDeviationMeasure.class, new
		// MeasureSerializer());
		REALKD_MODULE.addDeserializer(ComputationMeasure.class, new ComputationMeasureDeserializer());
		REALKD_MODULE.addDeserializer(ModelDeviationMeasure.class, new ModelDeviationMeasureDeserializer());
	}

	private static final ObjectMapper DEFAULT_OBJECT_MAPPER = new ObjectMapper();
	static {
		DEFAULT_OBJECT_MAPPER.enableDefaultTypingAsProperty(DefaultTyping.OBJECT_AND_NON_CONCRETE, "type");
		DEFAULT_OBJECT_MAPPER.registerModule(new GuavaModule());
		DEFAULT_OBJECT_MAPPER.registerModule(REALKD_MODULE);
		DEFAULT_OBJECT_MAPPER.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
		DEFAULT_OBJECT_MAPPER.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);
	}

	public static <T> String toJson(T object) {
		try {
			return DEFAULT_OBJECT_MAPPER.writeValueAsString(object);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> String toPrettyJson(T object) {
		try {
			return DEFAULT_OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsString(object);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static <T> T fromJson(String json, Class<T> type) {
		try {
			return DEFAULT_OBJECT_MAPPER.readValue(json, type);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
