/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-15 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.sequences;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.util.stream.Collectors.toList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.base.Identifier;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.Parameters;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;
import de.unibonn.realkd.data.propositions.AttributeBasedProposition;
import de.unibonn.realkd.data.propositions.AttributeToPropositionsMapper;
import de.unibonn.realkd.data.propositions.DefaultTableBasedPropositionalLogic;
import de.unibonn.realkd.data.propositions.Proposition;
import de.unibonn.realkd.data.propositions.PropositionalContextFromTableBuilder;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.data.table.attribute.DefaultDateAttribute;
import de.unibonn.realkd.data.table.attribute.DefaultMetricAttribute;

/**
 *
 * 
 * @author Sandy Moens
 * 
 * @since 0.3.2
 * 
 * @version 0.5.2
 *
 */
public class SequentialPropositionalContextFromTableFactory implements ParameterContainer {

	private static final Logger LOGGER = Logger.getLogger(PropositionalContextFromTableBuilder.class.getName());

	private static final SimpleDateFormat dt = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

	private SubCollectionParameter<AttributeToPropositionsMapper, Set<AttributeToPropositionsMapper>> attributeFactories;

	private Optional<String> id = Optional.empty();

	private Optional<String> name = Optional.empty();

	private String groupingAttributeName;
	private String distanceAttributeName;

	public SequentialPropositionalContextFromTableFactory() {
		this.attributeFactories = Parameters.subSetParameter(Identifier.identifier("Attribute_mappers"),
				"Attribute mappers",
				"Mappers that are used to create binary propositions for pattern mining from the data table",
				() -> PropositionalContextFromTableBuilder.ALL_MAPPERS,
				() -> PropositionalContextFromTableBuilder.DEFAULT_MAPPERS);

		this.distanceAttributeName = "id";
		this.distanceAttributeName = "date";
	}

	public SequentialPropositionalContextFromTableFactory name(String name) {
		this.name = Optional.ofNullable(name);
		return this;
	}

	public SequentialPropositionalContextFromTableFactory id(String id) {
		this.id = Optional.ofNullable(id);
		return this;
	}

	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(this.attributeFactories);
	}

	/**
	 * 
	 * @return the externally accessible parameter of the attribute mappers
	 */
	public SubCollectionParameter<AttributeToPropositionsMapper, Set<AttributeToPropositionsMapper>> attributeMapperParamater() {
		return attributeFactories;
	}

	/**
	 * 
	 * @return the current set of attribute to propositions mappers to be used for
	 *         creating propositional logics from table
	 */
	public Set<AttributeToPropositionsMapper> mappers() {
		return attributeFactories.current();
	}

	/**
	 * 
	 * @param mappers
	 *            the collection of mappers to be used for creating propositional
	 *            logics from table
	 */
	public void mappers(Set<AttributeToPropositionsMapper> mappers) {
		attributeFactories.set(mappers);
	}

	/**
	 * Sets the name of the attribute that is used for grouping sequences
	 * 
	 * @param groupingAttributeName
	 *            the name of the grouping attribute in the data table
	 */
	public void groupingAttributeName(String groupingAttributeName) {
		this.groupingAttributeName = groupingAttributeName;
	}

	/**
	 * Sets the name of the attribute that is used for annotating the distance
	 * between events in a sequences
	 * 
	 * @param distanceAttributeName
	 *            the name of the distance attribute in the data table
	 */
	public void distanceAttributeName(String distanceAttributeName) {
		this.distanceAttributeName = distanceAttributeName;
	}

	public SequentialPropositionalContext build(DataTable dataTable) {
		DefaultTableBasedPropositionalLogic propositionalLogic = compilePropositions(dataTable);
		Map<String, SequenceTransaction> sequences = compileSequences(dataTable, propositionalLogic);

		return DefaultTableBasedSequentialPropositionalContext.newSequenceDatabase(dataTable,
				newArrayList(sequences.values()), propositionalLogic.propositions());
	}

	private DefaultTableBasedPropositionalLogic compilePropositions(DataTable dataTable) {
		LOGGER.fine("Compiling proposition list");
		List<AttributeBasedProposition<?>> propositions = new ArrayList<>();
		for (Attribute<?> attribute : dataTable.attributes()) {
			if (this.groupingAttributeName.equals(attribute.caption())
					|| this.distanceAttributeName.equals(attribute.caption())) {
				continue;
			}
			for (AttributeToPropositionsMapper mapper : this.attributeFactories.current()) {
				propositions.addAll(mapper.constructPropositions(dataTable, attribute));
			}
		}

		propositions.forEach(p -> {
			propositions.subList(0, propositions.indexOf(p)).forEach(q -> {
				if (p.implies(q)) {
					LOGGER.fine("'" + p + "' implies already present proposition '" + q + "'");
				}
				if (q.implies(p)) {
					LOGGER.fine("'" + p + "' is implied by already present proposition '" + q + "'");
				}
			});
		});

		LOGGER.info("Done compiling proposition list (" + propositions.size() + " propositions added)");

		return new DefaultTableBasedPropositionalLogic(dataTable, propositions,
				id.orElse("statements_about_" + dataTable.identifier()),
				name.orElse("Statements about " + dataTable.name()),
				"Propositional logic generated using mappers: " + mappers());
	}

	private Map<String, SequenceTransaction> compileSequences(DataTable dataTable,
			DefaultTableBasedPropositionalLogic propositionalLogic) {
		LOGGER.fine("Creating sequences");

		Map<String, SequenceTransaction> sequences = newHashMap();

		List<Proposition> propositions = propositionalLogic.propositions();

		Attribute<?> groupingAttribute = dataTable
				.attribute(dataTable.attributeNames().indexOf(this.groupingAttributeName));
		Attribute<?> distanceAttribute = dataTable
				.attribute(dataTable.attributeNames().indexOf(this.distanceAttributeName));

		for (int i = 0; i < dataTable.population().size(); i++) {
			// Skip if there is no value for the grouping or distance attribute
			Optional<?> groupingValue = groupingAttribute.getValues().get(i);
			if (!groupingValue.isPresent()) {
				continue;
			}

			Optional<?> distValue = distanceAttribute.getValues().get(i);
			if (!distValue.isPresent()) {
				continue;
			}

			Set<Integer> truthSet = propositionalLogic.truthSet(i);

			Comparable<?> object = getValue(distValue.get(), distanceAttribute);

			if (object == null) {
				continue;
			}

			List<Proposition> eventList = truthSet.stream().map(e -> propositions.get(e)).collect(toList());
			SequenceEvent<?> event = SequenceEvents.newSequenceEvent(object, eventList);

			String groupingId = groupingValue.get().toString();
			SequenceTransaction sequence = sequences.get(groupingId);
			if (sequence == null) {
				sequences.put(groupingId, sequence = new SequenceTransaction());
			}
			sequence.addEvent(event);
		}

		LOGGER.info("Done creating sequences (" + sequences.size() + " sequences created)");

		return sequences;
	}

	private Comparable<?> getValue(Object object, Attribute<?> distanceAttribute) {
		if (distanceAttribute instanceof DefaultDateAttribute) {
			try {
				return dt.parse(object.toString());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return null;
		} else if (distanceAttribute instanceof DefaultMetricAttribute) {
			return (Double) object;
		}
		return null;
	}

}
