/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014-16 The Contributors of the realKD Project
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */
package de.unibonn.realkd.data.sequences;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newTreeSet;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import de.unibonn.realkd.data.propositions.Proposition;

/**
 * @author Sandy Moens
 * 
 * @since 0.5.3
 * 
 * @version 0.5.3
 *
 */
public class SequenceTransaction {

	private Set<SequenceEvent<?>> events =  newTreeSet();
	
	public SequenceTransaction() {
		;
	}
	
	public Set<SequenceEvent<?>> events() {
		return ImmutableSet.copyOf(this.events);
	}
	
	/**
	 * Inserts an event chronologically to the transaction if the time indication of the event
	 * does not yet exist. If the time indication already exists, the event at the specified time
	 * indication is appended with the new information.
	 * 
	 * @param event the new event that has to be added to the sequence transaction
	 */
	public void addEvent(SequenceEvent<?> event) {
		Iterator<SequenceEvent<?>> it = this.events.iterator();
		while(it.hasNext()) {
			SequenceEvent<?> next = it.next();
			int compareTo = next.compareTo(event);
			if(compareTo  == 0) {
				List<Proposition> eventList = newArrayList();
				eventList.addAll(next.propositions());
				eventList.addAll(event.propositions());
				SequenceEvent<?> newEvent = SequenceEvents.newSequenceEvent(event.value(), eventList);
				it.remove();
				this.events.add(newEvent);
				return;
			} else if(next.compareTo(event) == -1) {
				break;
			}
		}
		this.events.add(event);
	}
	
	public String toString() {
		return this.events.toString();
	}
	
}
