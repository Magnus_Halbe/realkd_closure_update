/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2014 University of Bonn
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package de.unibonn.realkd.data;

import static com.google.common.base.Preconditions.checkElementIndex;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.ImmutableList;

import de.unibonn.realkd.common.IndexSet;
import de.unibonn.realkd.common.IndexSets;
import de.unibonn.realkd.common.JsonSerialization;
import de.unibonn.realkd.common.workspace.EntitySerialForm;
import de.unibonn.realkd.common.workspace.HasSerialForm;
import de.unibonn.realkd.common.workspace.Workspace;

/**
 * Provides static factory methods for the creation of basic entities.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.3.0
 *
 */
public final class Populations {

	private static class DefaultObjectCollection
			implements Population, HasSerialForm<Population>, EntitySerialForm<Population> {

		private final ImmutableList<String> entityNames;

		private final String identifier;

		private final String name;

		private final String description;

		private final IndexSet indexSet;

		@JsonCreator
		private DefaultObjectCollection(@JsonProperty("identifier") String identifier,
				@JsonProperty("name") String name, @JsonProperty("description") String description,
				@JsonProperty("objectNames") List<String> entityNames) {
			this.identifier = identifier;
			this.name = name;
			this.description = description;
			this.entityNames = ImmutableList.copyOf(entityNames);

			this.indexSet = IndexSets.full(entityNames.size() - 1);
		}

		@Override
		public int size() {
			return entityNames.size();
		}

		@Override
		@JsonProperty
		public String identifier() {
			return identifier;
		}

		@Override
		@JsonProperty
		public String name() {
			return name;
		}

		@Override
		public IndexSet objectIds() {
			return indexSet;
		}

		@Override
		@JsonProperty
		public String description() {
			return description;
		}

		@Override
		public String objectName(int id) {
			checkElementIndex(id, size());
			return entityNames.get(id);
		}

		@Override
		@JsonProperty
		public List<String> objectNames() {
			return entityNames;
		}

		public String toString() {
			return identifier() + "::Population";
		}

		@Override
		public Population build(Workspace workspace) {
			return this;
		}

		@Override
		public EntitySerialForm<Population> serialForm() {
			return this;
		}

	}

	private Populations() {
		;
	}

	public static Population population(String identifier, String name, String description, List<String> entityNames) {
		return new DefaultObjectCollection(identifier, name, description, entityNames);
	}

	/**
	 * <p>
	 * Creates an anonymous population, i.e., one with object names generated
	 * from 0 to m-1, where m is desired number of objects.
	 * </p>
	 * <p>
	 * Like {@link #population(String, String, String, int)} but with default
	 * name and description.
	 * </p>
	 * 
	 * @param identifier
	 *            identifier of population
	 * @param numberOfObjects
	 *            number of objects/individuals in population
	 * @return population with desired properties
	 * 
	 */
	public static Population population(String identifier, int numberOfObjects) {
		List<String> objectNames = new ArrayList<>();
		for (int j = 0; j < numberOfObjects; j++) {
			objectNames.add(String.valueOf(j));
		}
		return population(identifier, identifier, "", objectNames);
	}

	/**
	 * Creates an anonymous population, i.e., one with object names generated
	 * from 0 to m-1, where m is desired number of objects.
	 * 
	 * @param identifier
	 *            identifier of population
	 * @param name
	 *            name of population
	 * @param description
	 *            description of population
	 * @param numberOfObjects
	 *            number of objects/individuals in population
	 * @return population with desired properties
	 */
	public static Population population(String identifier, String name, String description, int numberOfObjects) {
		List<String> objectNames = new ArrayList<>();
		for (int j = 0; j < numberOfObjects; j++) {
			objectNames.add(String.valueOf(j));
		}
		return population(identifier, name, description, objectNames);
	}

	public static void main(String[] args) {
		Population pop = population("id", "name", "descr", ImmutableList.of("bib", "bob", "bab"));
		System.out.println(pop);
		String json = JsonSerialization.toJson(pop);
		System.out.println(json);
		Population fromJson = JsonSerialization.fromJson(json, Population.class);
		System.out.println(fromJson);
	}

}
